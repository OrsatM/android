package edu.rit.maocad.roomdemo.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import edu.rit.maocad.roomdemo.data.dao.CustomerDao
import edu.rit.maocad.roomdemo.data.entities.Customer

@Database(entities = [Customer::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun customerDao(): CustomerDao

    companion object {
        //The @Volatile annotation will mark the JVM backing
        // field of the annotated property as volatile. Thus,
        // the writes to this field are immediately made visible
        // to other threads
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext,
                    AppDatabase::class.java, "customers")
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}