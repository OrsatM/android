package edu.rit.maocad.roomdemo.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import edu.rit.maocad.roomdemo.data.entities.Customer

@Dao
interface CustomerDao {
    @Query("SELECT * FROM customer")
    fun fetchAllCustomer(): LiveData<List<Customer>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCustomer(customer: Customer)

    @Query("DELETE FROM Customer where id = :id")
    suspend fun deleteCustomerById(id: Int)

    @Query("DELETE FROM customer")
    suspend fun deleteAllCustomer()
}