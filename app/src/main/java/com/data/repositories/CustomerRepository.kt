package edu.rit.maocad.roomdemo.data.repositories

import android.app.Application
import androidx.lifecycle.LiveData
import edu.rit.maocad.roomdemo.data.AppDatabase
import edu.rit.maocad.roomdemo.data.dao.CustomerDao
import edu.rit.maocad.roomdemo.data.entities.Customer

class CustomerRepository(application: Application) {
    private var customerDao: CustomerDao

    init {
        val database = AppDatabase.getDatabase(application)
        customerDao = database.customerDao()
    }


    val readAllCustomer: LiveData<List<Customer>> = customerDao.fetchAllCustomer()
    suspend fun insertUser(customer: Customer) {
        customerDao.insertCustomer(customer)
    }

    suspend fun deleteCustomerById(id: Int) {
        customerDao.deleteCustomerById(id)
    }

    suspend fun deleteAllCustomer() {
        customerDao.deleteAllCustomer()
    }

    init {
        val database = AppDatabase.getDatabase(application)
        customerDao = database.customerDao()
    }
}