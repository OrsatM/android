package edu.rit.maocad.roomdemo.models

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import edu.rit.maocad.roomdemo.data.entities.Customer
import edu.rit.maocad.roomdemo.data.repositories.CustomerRepository
import kotlinx.coroutines.launch

class CustomerViewModel(appObj: Application) : AndroidViewModel(appObj) {

    private val customerRepository: CustomerRepository = CustomerRepository(appObj)
    fun fetchAllCustomer(): LiveData<List<Customer>> {
        return customerRepository.readAllCustomer
    }

    fun insertCustomer(customer: Customer) {
        viewModelScope.launch {
            customerRepository.insertUser(customer = customer)
        }

    }

    fun deleteCustomerById(id: Int) {
        viewModelScope.launch {
            customerRepository.deleteCustomerById(id)
        }

    }
}